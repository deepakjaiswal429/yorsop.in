from django.shortcuts import render
from .models import Product_details

# Create your views here.
def index(request):
    product_details = Product_details.objects.all()
    return render(request, 'home/index.html', {'product_details':product_details})